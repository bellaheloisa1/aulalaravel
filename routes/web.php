<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('principal');
});
Route::get('/pagina1', function () {
    return view('pagina1');
});
Route::get('/pricipalcontroller::class', function () {
    return view('pagina2');
});
Route::get('/', function () {
    return view('principalcontroller');
});